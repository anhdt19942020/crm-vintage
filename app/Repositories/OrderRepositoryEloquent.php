<?php


namespace App\Repositories;


use App\Models\Order;
use App\Validators\OrderValidator;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

class OrderRepositoryEloquent extends BaseRepository implements OrderRepository
{
    /**
     * Specify Model class namel
     *
     * @return string
     */
    public function model()
    {
        return Order::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function index(Request $request)
    {
        $keyword = $request->get('keyword', '');
        $user = auth()->user();
        $orders = $this->getModel()->newQuery()->with(['addOnOrders', 'transactions', 'vehicles' => function ($q) {
            $q->select(['vehicles.id as vehicle_id', 'name', 'license']);
        }, 'store:id,store_name', 'orderItems.vehicle', 'customer:id,name,phone']);

        if ($store_id = $request->get('store_id')) {
            $orders->where('store_id', $store_id);
        }
        if ($user->role_rel->slug !== 'quan-tri-vien') {
            $orders->where('store_id', $user->store_id);
        }
        if ($request->filled('start_date')) {
            $start_date = $request->get('start_date');
            $orders = $orders->whereDate('created_at', '>=', $start_date);
        }
        if ($request->filled('end_date')) {
            $end_date = $request->get('end_date');
            $orders = $orders->whereDate('created_at', '<=', $end_date);
        }

        if ($request->filled('order_status')) {
            $orders->where('order_status', $request->get('order_status'));
        }

        if ($request->filled('is_out_of_date') && $request->get('is_out_of_date') == "true") {
            $orders->where('out_dated_at', '>', 0);
        }
        if ($keyword) {
            $orders->whereHas('customer', function ($query) use ($keyword) {
                $query->where('name', 'LIKE', '%' . $keyword . '%')
                    ->orWhere('phone', 'LIKE', '%' . $keyword . '%');
            })->orWhereHas('orderItems', function ($items) use ($keyword) {
                $items->whereHas('vehicle', function ($vehicle) use ($keyword) {
                    $vehicle->where('license', 'LIKE', '%' . $keyword . '%');
                });
            });
        }

        if ($request->get('is_all')) {
            return $orders->get();
        }


        return $orders->orderBy('id', 'desc')->paginate(config('app.paginate', 20));
    }

    public function store(array $params)
    {
        return $this->getModel()->newQuery()->create($params);
    }
}
