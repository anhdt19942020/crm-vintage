<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AddOnOrderRepository.
 *
 * @package namespace App\Repositories;
 */
interface AddOnOrderRepository extends RepositoryInterface
{
    //
}
