<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SellOrderItemRepository.
 *
 * @package namespace App\Repositories;
 */
interface SellOrderItemRepository extends RepositoryInterface
{
    //
}
