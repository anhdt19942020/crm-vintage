<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SellOrderRepository.
 *
 * @package namespace App\Repositories;
 */
interface SellOrderRepository extends RepositoryInterface
{
    //
}
