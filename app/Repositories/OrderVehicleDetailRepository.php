<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderVehicleDetailRepository.
 *
 * @package namespace App\Repositories;
 */
interface OrderVehicleDetailRepository extends RepositoryInterface
{
    //
}
