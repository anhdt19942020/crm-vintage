<?php

namespace App\Helpers;

use App\Models\PriceVehicle;
use App\Models\Vehicle;
use Carbon\Carbon;

class CarRentalHelper
{
    public static function getOutDate($order)
    {
        $return_at = $order->orderItems->pluck('return_at');
        $now = Carbon::now();
        $date_min = $now;
        foreach ($return_at as $date) {
            $dateFormat = Carbon::createFromFormat('Y-m-d H:i:s', $date);
            if ($dateFormat->lt($date_min)) {
                $date_min = $dateFormat;
            }
        }
        $difference = $date_min->diffInMinutes($now);
        return $difference > 0 ? $difference : 0;
    }

    public static function converMinutesInDay($minutes): string
    {
        if ($minutes > 0 && $minutes < 60) {
            return "Đến giờ trả xe";
        }
        $d = floor($minutes / 1440);
        $h = floor(($minutes - $d * 1440) / 60);
        $m = $minutes - ($d * 1440) - ($h * 60);
        $string = '';
        if ($d) {
            $string .= $d . ' Ngày ';
        }
        if ($h) {
            $string .= $h . ' giờ ';
        }
        if ($m) {
            $string .= $m . ' phút';
        }
        return $string;
    }

    public static function totalMoneyLateVehicle(Vehicle $vehicle, $hours, $price_type = 'day')
    {
        $day = floor($hours / 24);
        $remainHours = $hours % 24;
        $priceVehicle = PriceVehicle::query()->where('type', $vehicle->type)->where('price_type', $price_type)->first();
        $moneyLateHours = 0;
        if ($remainHours > 0 && $remainHours < 8)
            switch ($vehicle->type) {
                case 'xeso' :
                case 'xega' :
                    $moneyLateHours = $remainHours * 15000;
                    break;
                case 'xecon' :
                    $moneyLateHours = $remainHours * 25000;
                    break;
                case 'sh':
                    $moneyLateHours = $remainHours * 35000;
                    break;
            } else if ($remainHours >= 8) {
            $day += 1;
        }
        $moneyLateDate = $priceVehicle->price * $day;
        return $moneyLateDate + $moneyLateHours;
    }
}
