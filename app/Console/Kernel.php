<?php

namespace App\Console;

use App\Console\Commands\CaculateMinuteLateOrder;
use App\Console\Commands\ConverAddOnToTransaction;
use App\Console\Commands\DeleteOrderItemNotExistOrder;
use App\Console\Commands\SettingCommand;
use App\Console\Commands\SyncCustomerCommand;
use App\Console\Commands\TestCrontab;
use App\Console\Commands\UpdateOrderData;
use App\Console\Commands\UpdateTransactionTableData;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        UpdateOrderData::class,
        SyncCustomerCommand::class,
        UpdateTransactionTableData::class,
        CaculateMinuteLateOrder::class,
        TestCrontab::class,
        SettingCommand::class,
        DeleteOrderItemNotExistOrder::class,
        ConverAddOnToTransaction::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
         $schedule->command('command:crontab')->everyMinute();
         $schedule->command('feature:calculate-minute-late-orders')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
