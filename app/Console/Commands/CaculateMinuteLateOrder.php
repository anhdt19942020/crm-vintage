<?php

namespace App\Console\Commands;

use App\Helpers\CarRentalHelper;
use App\Models\Order;
use App\Validators\OrderValidator;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CaculateMinuteLateOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feature:calculate-minute-late-orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tính số phút muộn của order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        info('Quét đơn hàng quá hạn');
        $orders = Order::query()->where('order_status', OrderValidator::ORDER_RENTING);
        $total = (clone($orders))->get()->count();
        $this->output->progressStart($total / 500);
        $orders->chunkById(500, function ($items) {
            foreach ($items as $order) {
                $outDate = CarRentalHelper::getOutDate($order);
                // 30 phút mới tính quá hạn
                if ($outDate < 30) {
                    $order->update([
                        'out_dated_at' => 0
                    ]);
                    continue;
                }
                $order->update([
                    'out_dated_at' => $outDate
                ]);
                $orderItems = $order->orderItems;
                $now = Carbon::now();
                $totalOrder = 0;
                foreach ($orderItems as $orderItem) {
                    $dateFormat = Carbon::createFromFormat('Y-m-d H:i:s', $orderItem->return_at);
                    if ($dateFormat->lt($now)) {
                        if ($orderItem->vehicle) {
                            $differenceMinute = $now->diffInMinutes($dateFormat);
                            $moneyLate = CarRentalHelper::totalMoneyLateVehicle($orderItem->vehicle, round($differenceMinute / 60));
                            $orderItem->update([
                                'money_out_date' => $moneyLate,
                                'minute_out_date' => $differenceMinute,
                            ]);
                            $order->update(['total' => $totalOrder + $moneyLate + ($orderItem->handler_price ?: $orderItem->total_money)]);
                        }
                    }
                }
            }
            $this->output->progressAdvance();
        }, 'id');
        dump('done orders table');
    }
}
