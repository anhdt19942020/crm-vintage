<?php

namespace App\Console\Commands;

use App\Entities\AddOnOrder;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ConverAddOnToTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:convertAddonToTransaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $addOns = AddOnOrder::all();
        foreach ($addOns as $item) {
            $data = [
                'order_id' => $item->order_id,
                'value' => $item->price,
                'user_id' => $item->user_id,
                'name' => 'addon',
                'type' => 'addon',
                'note' => 'Gia hạn thêm  cho hợp đồng: ' . $item->order_id,
                'status' => 'approved',
                'store_id' => $item->order->store_id,
                'created_at' => Carbon::parse($item->date)->timezone('Asia/Ho_Chi_Minh'),
                'updated_at' => Carbon::parse($item->date)->timezone('Asia/Ho_Chi_Minh')
            ];
            Transaction::create($data);
        }
        echo "done";
    }
}
