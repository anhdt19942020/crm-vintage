<?php

namespace App\Console\Commands;

use App\Entities\Customer;
use App\Models\Order;
use App\Models\PriceVehicle;
use App\Models\Vehicle;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateOrderData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update-orders:new-data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cập nhật dữ liệu mới cho bảng orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::query()
            ->whereNotNull('rent_at')
            ->whereNotNull('vehicle_ids')
            ->whereNotNull('return_at');
        $total = (clone($orders))->get()->count();
        $this->output->progressStart($total / 500);
        $orders->chunkById(500, function ($items) {
            foreach ($items as $order) {
                $dataCustomer = [
                    'name' => $order->customer_name,
                    'phone' => $order->customer_phone,
                    'address' => $order->customer_address,
                    'id_card' => $order->customer_idnumber ?: mt_rand(1000000, 9999999),
                ];
                $customer = Customer::query()->where('id_card', $order->customer_idnumber)->first();
                if (!$customer){
                    $customer = Customer::query()->create($dataCustomer);
                }
                $order->update(['customer_id' => $customer->id]);
                $total = 0;
                if ($order->vehicle_ids) {
                    $vehicle_ids = json_decode($order->vehicle_ids, true);
                    $metaData = json_decode($order->metadata, true);
                    $order->update([
                        'pid' => $metaData['deposit'] ?: 0,
                        'note' => $metaData['note'] ?: '',
                    ]);
                    foreach ($vehicle_ids as $vehicle_id) {
                        $vehicle = Vehicle::query()->find($vehicle_id);
                        $start = new Carbon($order->return_at);
                        $end = new Carbon($order->rent_at);
                        $diffDate = $start->diffInDays($end);
                        if ($metaData['fee']) {
                            $type = 'total';
                        } else {
                            $type = 'day';
                        }
                        if ($vehicle){
                            $priceVehicle = PriceVehicle::query()->where('type', $vehicle->type)
                                ->where('from_date', '<=', $diffDate)
                                ->where('price_type', $type)
                                ->where('to_date', '>=', $diffDate)->first();
                        }
                        $total += $priceVehicle->price ?? 0;
                        $order->vehicles()->attach($vehicle_id, [
                            'price_id' => $priceVehicle->id ?? 0,
                            'rent_at' => $order->rent_at,
                            'return_at' => $order->return_at,
                            'total_money' => $priceVehicle->price ?? 0,
                            'borrow_hats' => $metaData['borrow_a_hat'] ?: 0,
                            'type' => $type,
                        ]);
                    }
                    $order->update([
                        'total' => $metaData['fee'] ?: $total,
                    ]);
                }
            }
            $this->output->progressAdvance();
        }, 'id');
        dump('Xong orders');
    }
}
