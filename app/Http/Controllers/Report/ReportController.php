<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Http\Services\ReportService;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    private $reportService;

    public function __construct(ReportService $reportService)
    {
        $this->reportService = $reportService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reportCarRental(Request $request)
    {
        $transactions = $this->reportService->reportCarRental($request->all());
        $result = $this->reportService->handleReportCarRental($transactions);
        return $this->successResponse($result);
    }

}
