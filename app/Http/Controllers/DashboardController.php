<?php

namespace App\Http\Controllers;

use App\Http\Services\DashboardService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    private $dashboardService;

    public function __construct(DashboardService $dashboardService)
    {
        $this->dashboardService = $dashboardService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function report(Request $request)
    {
        return $this->successResponse($this->dashboardService->report());
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function reportChart(Request $request)
    {
        $now = Carbon::now();
        $startDate = $now->copy()->startOfMonth();
        $endDate = $now;
        return $this->successResponse($this->dashboardService->reportChart($startDate, $endDate));
    }
}
