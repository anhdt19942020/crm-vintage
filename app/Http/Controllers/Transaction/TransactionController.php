<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\Controller;
use App\Http\Services\TransactionService;
use App\Models\Transaction;

class TransactionController extends Controller
{
    private $transactionService;

    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    public function index()
    {
        return $this->successResponse($this->transactionService->getList());
    }

    /**
     * @param Transaction $transaction
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Transaction $transaction)
    {
        return $this->successResponse($transaction->delete(), 'Xóa thành công');
    }
}
