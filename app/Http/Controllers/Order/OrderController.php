<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Http\Services\OrderService;
use App\Models\Order;
use App\Models\Vehicle;
use App\Validators\OrderValidator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    protected $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    public function getOrderCarRental(Request $request): JsonResponse
    {
        $result = Order::with(['store'])->paginate(15);
        $result->map(function ($value) {
            $vehicle_ids = json_decode($value->vehicle_ids, true);
            $vehicles = Vehicle::whereIn('id', $vehicle_ids)->get();
            data_set($value, 'vehicles', $vehicles);
            return $value;
        });
        return $this->successResponse($result);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $orders = $this->orderService->index($request);
        return $this->successResponse(OrderResource::collection($orders));
    }

    /**
     * @param Request $request
     * @param Order $order
     * @return JsonResponse
     */
    public function show(Request $request, Order $order): JsonResponse
    {
        return $this->successResponse($order->load(['addOnOrders.user:id,name', 'customer', 'vehicles', 'store', 'orderItems', 'transactions', 'activityLogs.user:id,name']));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function report(Request $request): JsonResponse
    {
        $request->merge(['is_all' => true]);
        $result = $this->orderService->index($request);
        $getAddOn = $this->orderService->getAddOn($request);
        return $this->successResponse($this->orderService->report($result, $getAddOn));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $request->validate(OrderValidator::store());
        try {
            DB::beginTransaction();
            $this->orderService->store($request);
            DB::commit();
            return $this->successResponse('', 'Thêm mới thành công');
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->errorResponse($exception->getMessage(), 422);
        }
    }

    /**
     * @param Request $request
     * @param Order $order
     * @return JsonResponse
     */
    public function update(Request $request, Order $order): JsonResponse
    {
        $request->validate(OrderValidator::update($request, $order));
        try {
            DB::beginTransaction();
            $this->orderService->update($request, $order);
            DB::commit();
            return $this->successResponse('', 'Cập nhật thành công');
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->errorResponse($exception->getMessage(), 422);
        }
    }

    /**
     * @param Request $request
     * @param Order $order
     * @return JsonResponse
     */
    public function deposit(Request $request, Order $order): JsonResponse
    {
        $request->validate(OrderValidator::deposit());
        try {
            DB::beginTransaction();
            $this->orderService->deposit($order);
            DB::commit();
            return $this->successResponse('', 'Thêm mới thành công');
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->errorResponse($exception->getMessage(), 422);
        }
    }

    /**
     * @param Request $request
     * @param Order $order
     * @return JsonResponse
     */
    public function complete(Request $request, Order $order): JsonResponse
    {
        try {
            DB::beginTransaction();
            $this->orderService->complete($request, $order);
            $this->orderService->updateVehicle($order);
            DB::commit();
            return $this->successResponse('', 'Cập nhật thành công');
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->errorResponse($exception->getMessage(), 422);
        }
    }

    /**
     * @param Order $order
     * @return JsonResponse
     */
    public function destroy(Order $order): JsonResponse
    {
        try {
            DB::beginTransaction();
            $order->addOnOrders()->delete();
            $orderItems = $order->orderItems();
            $vehicleIds = $orderItems->pluck('vehicle_id')->toArray();
            //Update vehicle
            Vehicle::whereIn('id', $vehicleIds)->update([
                'status' => Vehicle::STATUS_READY
            ]);
            $order->delete();
            DB::commit();
            return $this->successResponse('', 'Xóa thành công');
        } catch (\Exception $exception) {
            DB::rollBack();
            return $this->errorResponse($exception->getMessage(), 422);
        }
    }

    public function addOnPrice(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required|integer',
            'price' => 'required|integer|min:1',
            'return_at' => 'required|date',
        ], [
            'price.min' => 'Tiền không được để trống',
            'return_at.required' => 'Ngày trả không được để trống'
        ]);
        $user = Auth::user();
        if (!$user) {
            return $this->errorResponse('Bạn cần đăng nhập lại', 404);
        }
        $addOn = $this->orderService->addOnPrice($request->order_id, $request->price, $user->id);
        $this->orderService->updateReturnAt($request->order_id, $request->return_at);
        if (!$addOn) {
            return $this->errorResponse('Có lỗi xảy ra', 404);
        }
        return $this->successResponse($addOn, 'Nạp tiền gia hạn thành công');
    }


}
