<?php

namespace App\Http\Services;

use App\Interfaces\ICrud;
use App\Models\Vehicle;
use App\Repositories\VehicleRepository;

class VehicleService implements ICrud
{
    private $vehicleRepository;

    public function __construct(VehicleRepository $vehicleRepository)
    {
        $this->vehicleRepository = $vehicleRepository;
    }

    public function index(array $params, $all = false)
    {
        $limit = data_get($params, 'limit', config('app.paginate'));
        $items = $this->vehicleRepository->with(['store:id,store_name'])
            ->filter($params)
            ->orderBy('id', 'DESC');
        if ($all) {
            return $items->get();
        }
        return $items->paginate($limit);
    }

    public function report($items)
    {
        $total_vehicle = $items->where('status', '!=', Vehicle::STATUS_SOLD)->count();
        $total_vehicle_ready = $items->where('status', Vehicle::STATUS_READY)->count();
        $total_vehicle_using = $items->where('status', Vehicle::STATUS_USING)->count();
        $total_vehicle_ga = $items->where('status', '!=', Vehicle::STATUS_SOLD)->where('type', Vehicle::TYPE_XEGA)->count();
        $total_vehicle_so = $items->where('status', '!=', Vehicle::STATUS_SOLD)->where('type', Vehicle::TYPE_XESO)->count();
        $total_vehicle_con = $items->where('status', '!=', Vehicle::STATUS_SOLD)->where('type', Vehicle::TYPE_XECON)->count();
        $total_price = $items->where('status', '!=', Vehicle::STATUS_SOLD)->sum('cost_price');

        return [
            'total_vehicle' => $total_vehicle,
            'total_vehicle_ready' => $total_vehicle_ready,
            'total_vehicle_using' => $total_vehicle_using,
            'total_vehicle_ga' => $total_vehicle_ga,
            'total_vehicle_so' => $total_vehicle_so,
            'total_vehicle_con' => $total_vehicle_con,
            'total_price' => $total_price
        ];
    }

    public function all()
    {
        // TODO: Implement all() method.
    }

    public function store(array $params)
    {
        return $this->vehicleRepository->create($params);
    }

    public function update($id, array $params)
    {
        return $this->vehicleRepository->update($params, $id);
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }

    public function updateStatus(int $vehicle_id, string $status)
    {
        return $this->vehicleRepository->where('id', $vehicle_id)->update(['status' => $status]);
    }

    public function checkStatusOtherSold($id)
    {
        return $this->vehicleRepository->where('id', $id)->where('status', '!=', Vehicle::STATUS_SOLD)->first();
    }
}
