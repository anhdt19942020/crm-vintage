<?php

namespace App\Http\Services;

use App\Interfaces\ICrud;
use App\Models\Transaction;
use App\Models\Vehicle;
use App\Repositories\CustomerRepository;
use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use App\Repositories\VehicleRepository;
use App\Validators\OrderValidator;
use Carbon\Carbon;

class DashboardService
{
    private $vehicleRepository;
    private $customerRepository;
    private $userRepository;
    private $orderRepository;

    public function __construct(
        VehicleRepository  $vehicleRepository,
        CustomerRepository $customerRepository,
        UserRepository     $userRepository,
        OrderRepository    $orderRepository
    )
    {
        $this->vehicleRepository = $vehicleRepository;
        $this->customerRepository = $customerRepository;
        $this->userRepository = $userRepository;
        $this->orderRepository = $orderRepository;
    }

    public function reportChart(Carbon $startDate, Carbon $endDate)
    {
        $labels = [];
        $values = [];
        $diffInDays = $endDate->diffInDays($startDate);
        for ($i = 0; $i <= $diffInDays; $i++) {
            $day = $startDate->copy()->addDays($i);
            $total = $this->orderRepository->whereBetween('created_at', [$day->copy()->startOfDay(), $day->copy()->endOfDay()])->sum('total');
            $addOns = Transaction::whereBetween('created_at', [$day->copy()->startOfDay(), $day->copy()->endOfDay()])->sum('value');
            array_push($labels, $day->format('d-m-Y'));
            array_push($values, $total + $addOns);
        }

        return [
            'labels' => $labels,
            'values' => $values
        ];
    }

    public function report()
    {
        $now = Carbon::now();
        $start_day = $now->copy()->startOfDay();
        $end_day = $now->copy()->endOfDay();
        $startOfMonth = $now->copy()->startOfMonth();
        $endOfMonth = $now->copy()->endOfMonth();


        $total_vehicle = $this->vehicleRepository->count();
        $total_vehicle_using = $this->vehicleRepository->where('status', Vehicle::STATUS_USING)->count();
        $total_vehicle_ready = $this->vehicleRepository->where('status', Vehicle::STATUS_READY)->count();
        $total_vehicle_repairing = $this->vehicleRepository->where('status', Vehicle::STATUS_REPAIRING)->count();
        $total_vehicle_broken = $this->vehicleRepository->where('status', Vehicle::STATUS_BROKEN)->count();

        $total_customer = $this->customerRepository->count();
        $total_staff = $this->userRepository->where('role_id', '!=', 1)->count();
        $total_order_in_day = $this->orderRepository->findWhereBetween('created_at', [$start_day, $end_day])->count();
        $total_order_in_month = $this->orderRepository->findWhereBetween('created_at', [$startOfMonth, $endOfMonth])->count();
        $total_order_out_date_in_month = $this->orderRepository->findWhereBetween('created_at', [$startOfMonth, $endOfMonth])
            ->where('out_date_at', '>', 0)
            ->where('order_status', OrderValidator::ORDER_RENTING)
            ->count();
        $total_profit_in_day = $this->orderRepository->findWhereBetween('created_at', [$start_day, $end_day])->sum('total');
        $total_profit_in_month = $this->orderRepository->findWhereBetween('created_at', [$startOfMonth, $endOfMonth])->sum('total');


        return [
            //Vehicle
            'total_vehicle' => $total_vehicle,
            'total_vehicle_ready' => $total_vehicle_ready,
            'total_vehicle_using' => $total_vehicle_using,
            'total_vehicle_repairing' => $total_vehicle_repairing,
            'total_vehicle_broken' => $total_vehicle_broken,
            //customer
            'total_customer' => $total_customer,
            'total_staff' => $total_staff,
            // Order
            'total_order_in_month' => $total_order_in_month,
            'total_order_in_day' => $total_order_in_day,
            'total_order_out_date_in_month' => $total_order_out_date_in_month,
            //Doanh thu
            'total_profit_in_day' => $total_profit_in_day,
            'total_profit_in_month' => $total_profit_in_month,
        ];
    }
}
