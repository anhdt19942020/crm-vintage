<?php


namespace App\Http\Services;


use App\Entities\Customer;
use App\Models\ActivityLog;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\Vehicle;
use App\Repositories\ActivityLogRepository;
use App\Repositories\AddOnOrderRepository;
use App\Repositories\CustomerRepository;
use App\Repositories\OrderRepository;
use App\Repositories\TransactionRepository;
use App\Repositories\VehicleRepository;
use App\Validators\OrderValidator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderService
{
    protected $orderRepository;
    protected $customerRepository;
    protected $transactionRepository;
    protected $activityLogRepository;
    protected $vehicleRepository;
    protected $addOnOrderRepository;

    public function __construct(
        OrderRepository       $orderRepository,
        CustomerRepository    $customerRepository,
        TransactionRepository $transactionRepository,
        ActivityLogRepository $activityLogRepository,
        VehicleRepository     $vehicleRepository,
        AddOnOrderRepository  $addOnOrderRepository
    )
    {
        $this->orderRepository = $orderRepository;
        $this->customerRepository = $customerRepository;
        $this->transactionRepository = $transactionRepository;
        $this->activityLogRepository = $activityLogRepository;
        $this->vehicleRepository = $vehicleRepository;
        $this->addOnOrderRepository = $addOnOrderRepository;
    }

    public function index(Request $request)
    {
        return $this->orderRepository->index($request);
    }

    public function store(Request $request)
    {
        $dataCustomer = [
            'name' => $request->get('customer_name'),
            'phone' => $request->get('customer_phone'),
            'address' => $request->get('customer_address'),
            'id_card' => $request->get('customer_id_card'),
        ];
        $customer = Customer::query()->where('id_card', $request->get('customer_id_card'))->first();
        if (!$customer) {
            $customer = $this->customerRepository->skipPresenter()->create($dataCustomer);
        } else {
            $customer->update($dataCustomer);
        }
        $dataOrder = [
            'store_id' => $request->get('store_id'),
            'order_type' => OrderValidator::ORDER_TYPE_RENTING,
            'order_status' => OrderValidator::ORDER_RENTING,
            'total' => $request->get('total'),
            'pid' => $request->get('amount'),
            'note' => $request->get('note'),
            'note_payment' => $request->get('note_item'),
        ];
        $dataOrder['customer_id'] = $customer->id;
        $order = $this->orderRepository->store($dataOrder);
        $itemVehicles = $request->get('items');
        foreach ($itemVehicles as $itemVehicle) {
            $vehicle_id = $itemVehicle['vehicle_id'];
            Vehicle::where('id', $vehicle_id)->update(['status' => Vehicle::STATUS_USING]);
            $order->vehicles()->attach($itemVehicle['vehicle_id'],
                [
                    'price_id' => data_get($itemVehicle, 'price_id', 0),
                    'rent_at' => Carbon::parse($itemVehicle['rent_at']),
                    'return_at' => Carbon::parse($itemVehicle['return_at']),
                    'total_money' => data_get($itemVehicle, 'total_money', 0),
                    'borrow_hats' => $itemVehicle['borrow_hats'],
                    'type' => $itemVehicle['type'] ?? 'total',
                    'handler_price' => $itemVehicle['other_account'],
                ]);
        }
        if ($request->get('amount')) {
            $dataTransaction = [
                'name' => 'order:deposit:' . $order->id,
                'type' => 'in',
                'value' => $request->get('amount'),
                'note' => 'Đặt cọc hợp đồng ' . $order->id,
                'user_id' => Auth::id(),
                'order_id' => $order->id,
                'store_id' => $request->get('store_id'),
            ];
            $this->transactionRepository->create($dataTransaction);
        }
    }

    public function update(Request $request, Order $order)
    {
        $this->saveOrderLog($request, $order);
        $warning = $request->get('warning');
        $dataCustomer = [
            'name' => $request->get('customer_name'),
            'phone' => $request->get('customer_phone'),
            'address' => $request->get('customer_address'),
            'id_card' => $request->get('customer_id_card'),
            'warning' => $request->get('warning'),
            'status' => $warning ? Customer::STATUS_WARNING : Customer::STATUS_ACTIVE,
        ];
        $dataOrder = [
            'store_id' => $request->get('store_id'),
            'order_type' => OrderValidator::ORDER_TYPE_RENTING,
            'order_status' => OrderValidator::ORDER_RENTING,
            'total' => $request->get('total'),
            'pid' => $request->get('amount'),
            'note' => $request->get('note'),
            'note_payment' => $request->get('note_item'),
        ];
        $order->customer()->update($dataCustomer);
        if ($request->get('amount') > $order->pid) {
            $this->raiseDeposit($request, $order);
        }
        $order->update($dataOrder);
        $itemVehicles = $request->get('items');
        foreach ($itemVehicles as $k => $itemVehicle) {
            $itemVehicles[$itemVehicle['vehicle_id']] = [
                'price_id' => data_get($itemVehicle, 'price_id', 0),
                'rent_at' => Carbon::parse($itemVehicle['rent_at']),
                'return_at' => Carbon::parse($itemVehicle['return_at']),
                'total_money' => data_get($itemVehicle, 'total_money', 0),
                'borrow_hats' => $itemVehicle['borrow_hats'],
                'type' => $itemVehicle['type'] ?? 'total',
                'handler_price' => $itemVehicle['other_account'] ?? 0,
            ];
            unset($itemVehicles[$k]);
        }
        $order->vehicles()->sync($itemVehicles);
    }

    public function deposit(Order $order)
    {
        return $order->save();
    }

    private function raiseDeposit(Request $request, Order $order)
    {
        $note = 'Tăng tiền cọc hợp đồng';
        $type = 'in';
        $dataTransaction = [
            'name' => 'order:raise:' . $order->id,
            'type' => $type,
            'value' => abs($request->get('amount') - $order->pid),
            'note' => $note . $order->id,
            'user_id' => Auth::id(),
            'order_id' => $order->id,
            'store_id' => $order->store_id,
        ];
        $this->transactionRepository->create($dataTransaction);
    }

    public function complete(Request $request, Order $order)
    {
        $order->update([
            'order_status' => OrderValidator::ORDER_COMPLETED,
        ]);
        $note = $order->total > $order->pid ? 'Hoàn thành hợp đồng' : 'Hoàn thành hợp đồng và hoàn cọc';
        $type = $order->total > $order->pid ? 'in' : 'out';
        if (abs($order->total - $order->pid)) {
            $dataTransaction = [
                'name' => 'order:complete:' . $order->id,
                'type' => $type,
                'value' => abs($order->total - $order->pid),
                'note' => $note . $order->id,
                'user_id' => Auth::id(),
                'order_id' => $order->id,
                'store_id' => $order->store_id,
            ];
            $this->transactionRepository->create($dataTransaction);
        }
        $order->orderItems()->update(['completed_at' => Carbon::now()]);

    }

    public function updateVehicle(Order $order)
    {
        $idVehicles = $order->orderItems()->pluck('vehicle_id')->toArray();
        return $this->vehicleRepository->whereIn('id', $idVehicles)->update(['status' => Vehicle::STATUS_READY]);
    }

    private function saveOrderLog(Request $request, Order $order)
    {
        $log = new ActivityLog();
        $log->name = 'order:' . $order->id;
        $log->order_id = $order->id;
        $log->user_id = Auth::id();
        $log->action = 'update';
        $log->content = 'Sửa hợp đồng số ' . $order->id;
        $log->metadata = json_encode([
            'old_data' => [
                'store_id' => $order->store_id,
                'customer_name' => $order->customer->name,
                'customer_phone' => $order->customer->phone,
                'customer_address' => $order->customer->address,
                'customer_idnumber' => $order->customer->id_card,
                'vehicle_ids' => $order->vehicles()->get()->toArray(),
                'order_type' => $order->order_type,
                'order_status' => $order->status,
                'rent_at' => $order->rent_at,
                'die_time' => $order->die_time,
                'return_at' => $order->return_at,
            ]
        ]);
        $log->save();
    }

    public function getAddOn(Request $request)
    {
        $user = Auth::user();
        $addOns = $this->transactionRepository->where('type', 'addon');
        if ($request->filled('start_date')) {
            $start_date = $request->get('start_date');
            $addOns = $addOns->whereDate('created_at', '>=', $start_date);
        }
        if ($user->role_rel->slug !== 'quan-tri-vien') {
            $addOns->whereHas('user', function ($query) use ($user) {
                $query->where('store_id', $user->store_id);
            });
        }
        if ($request->filled('end_date')) {
            $end_date = $request->get('end_date');
            $addOns = $addOns->whereDate('created_at', '<=', $end_date);
        }
        if ($request->filled('store_id')) {
            $store_id = $request->store_id;
            $addOns->whereHas('order', function ($query) use ($store_id) {
                $query->where('store_id', $store_id);
            });
        }
        return $addOns->sum('value');
    }

    public function report($orders, $priceAddOn = 0): array
    {
        $orders->map(function ($order) {
            $transaction_out = $order->transactions->where('type', Transaction::CHI);
            $transaction_in = $order->transactions->where('type', Transaction::THU);
//            $transaction_addon = $order->transactions->where('type', Transaction::ADDON);
            $order->thu_price = 0;
            $order->refund_customer = 0;
//            if ($order->order_status == OrderValidator::ORDER_COMPLETED) {
//                $order->refund_customer = $transaction_out->sum('value');
//                $order->thu_price = $transaction_in->sum('value');
//            }
            $order->refund_customer = $transaction_out->sum('value');
            $order->thu_price = $transaction_in->sum('value');
//            $order->addon = $transaction_addon->sum('value');
            return $order;
        });
        $total_order = $orders->count();
        $total_contracts_completed = $orders->where('order_status', OrderValidator::ORDER_COMPLETED)->count();
        $total_contracts_renting = $orders->where('order_status', OrderValidator::ORDER_RENTING)->count();
        $total_out_of_date = $orders->where('out_dated_at', '>', 0)->where('order_status', '!=', OrderValidator::ORDER_COMPLETED)->count();
        $total_price = $orders->sum('thu_price') + $priceAddOn;
        $total_refund_customer = $orders->sum('refund_customer');
        $total_price_profit = $orders->sum('total') + $priceAddOn;
        return [
            'total_order' => $total_order,
            'total_contracts_completed' => $total_contracts_completed,
            'total_contracts_renting' => $total_contracts_renting,
            'total_out_of_date' => $total_out_of_date,
            'total_price' => $total_price,
            'total_refund_customer' => $total_refund_customer,
            'total_price_profit' => $total_price_profit,
        ];
    }

    /**
     * @param $order_id
     * @param $price
     * @param $user_id
     * @return mixed
     */
    public function addOnPrice($order_id, $price, $user_id)
    {
        return Transaction::create([
            'order_id' => $order_id,
            'value' => $price,
            'user_id' => $user_id,
            'name' => 'addon',
            'type' => 'addon',
            'note' => 'Gia hạn thêm  cho hợp đồng: ' . $order_id,
            'status' => 'approved',
        ]);
//        return $this->addOnOrderRepository->create([
//            'order_id' => $order_id,
//            'price' => $price,
//            'user_id' => $user_id,
//            'date' => Carbon::now(),
//        ]);
    }

    public function updateReturnAt($order_id, $date)
    {
        $order = $this->orderRepository->find($order_id);
        if (!$order) {
            return false;
        }

        return $order->orderItems()->update([
            'return_at' => Carbon::parse($date)->timezone('Asia/Ho_Chi_Minh')
        ]);
    }
}
