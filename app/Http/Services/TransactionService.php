<?php

namespace App\Http\Services;

use App\Repositories\TransactionRepository;

class TransactionService
{
    private $transactionRepository;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function getList()
    {
        return $this->transactionRepository->orderBy('id', 'DESC')->paginate(20);
    }

}
