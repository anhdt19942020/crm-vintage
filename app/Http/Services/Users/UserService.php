<?php

namespace App\Http\Services\Users;

use App\Interfaces\ICrud;
use App\Repositories\UserRepository;

class UserService implements ICrud
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function store(array $data)
    {
        return $this->userRepository->create($data);
    }

    public function index(array $params)
    {
        return $this->userRepository->with(['role_rel', 'store:id,store_name'])
            ->orderBy('id', 'DESC')
            ->paginate(20);
    }

    /**
     * @param $store_id
     * @return mixed
     */
    public function getByStore($store_id)
    {
        return $this->userRepository->with(['role_rel'])
            ->where('store_id', $store_id)
            ->orderBy('id', 'DESC')
            ->get(['id', 'name', 'role_id']);
    }

    public function update($id, array $params)
    {
        return $this->userRepository->update($params, $id);
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }

    public function all()
    {
        // TODO: Implement all() method.
    }
}
