<?php

namespace App\Http\Services;

use App\Models\Store;
use App\Models\Transaction;
use App\Repositories\TransactionRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ReportService
{
    private $transactionRepository;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    public function reportCarRental(array $params)
    {
        $store_id = data_get($params, 'store_id');
        $dates = data_get($params, 'dates', []);
        $transactions = $this->transactionRepository->select(
            DB::raw('day(created_at) as day'),
            DB::raw('month(created_at) as month'),
            DB::raw('year(created_at) as year'),
            'order_id', 'created_at', 'type', 'value', 'user_id', 'store_id'
        );
        if (!empty($dates)) {
            $transactions->where(function ($query) use ($dates) {
                $start_date = $dates[0];
                if ($start_date != 'null') {
                    $query->where('created_at', '>=', Carbon::createFromFormat('Y-m-d', $dates[0]))
                        ->where('created_at', '<=', Carbon::createFromFormat('Y-m-d', $dates[1]));
                }
            });
        }

        if ($store_id) {
            $transactions->where('store_id', $store_id);
        }
        return $transactions->orderBy('id', 'DESC')->get();

    }

    public function handleReportCarRental($transactions)
    {
        $storeAll = Store::all();
        $keyed = $storeAll->mapWithKeys(function ($item, $key) {
            return [$item['id'] => $item['store_name']];
        })->toArray();
        $result = [];
        $transaction_group = $transactions->groupBy('store_id');

        foreach ($transaction_group as $store_id => $items) {
            if (!array_key_exists($store_id, $keyed)) {
                continue;
            }
            $store_name = $keyed[$store_id];
            foreach ($items as $transaction) {
                $key = $transaction->day . '/' . $transaction->month . '/' . $transaction->year;
                $type = $transaction->type;
                $amount = $transaction->value;
                if (isset($result[$store_name][$key])) {
                    if ($type == Transaction::THU) {
                        $result[$store_name][$key][Transaction::THU] += $amount;
                    } else {
                        $result[$store_name][$key][Transaction::CHI] = $amount;
                    }
                } else {
                    if ($type == Transaction::THU) {
                        $result[$store_name][$key] = [
                            Transaction::THU => $amount,
                            Transaction::CHI => 0,
                        ];
                    } else {
                        $result[$store_name][$key] = [
                            Transaction::THU => 0,
                            Transaction::CHI => $amount,
                        ];
                    }
                }
                $result[$store_name][$key]['profit'] = $result[$store_name][$key][Transaction::THU] - $result[$store_name][$key][Transaction::CHI];
                $result[$store_name][$key]['date'] = $key;
            }
        }

        return $result;
    }
}
