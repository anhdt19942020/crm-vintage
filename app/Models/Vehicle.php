<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Vehicle extends Model
{
    const STATUS_READY = 'ready';
    const STATUS_PENDING = 'pending';
    const STATUS_USING = 'using';
    const STATUS_REPAIRING = 'repairing';
    const STATUS_SOLD = 'sold';
    const STATUS_BROKEN = 'broken';

    const TYPE_XESO = 'xeso';
    const TYPE_XEGA = 'xega';
    const TYPE_XECON = 'xecon';
    const TYPE_XE_SH = 'xesh';

    protected $fillable = [
        'name',
        'brand',
        'type',
        'year',
        'store_id',
        'license',
        'chassis',
        'engine',
        'status',
        'cost_price',
        'sale_price',
        'price_range',
        'created_by',
        'color',
        'type_of_service_id',
        'price_min',
        'price_max'
    ];

    public function store(): BelongsTo
    {
        return $this->belongsTo(Store::class, 'store_id', 'id');
    }

    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(Order::class, 'order_vehicle_details', 'vehicle_id', 'order_id');
    }
}
