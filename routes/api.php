<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\Customer\CustomerController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Order;
use App\Http\Controllers\Vehicle\PriceVehicleController;
use App\Http\Controllers\Order\OrderSellController;
use App\Http\Controllers\Vehicle\VehicleController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Store\StoreController;
use App\Http\Controllers\Transaction\TransactionController;
use App\Http\Controllers\Report\ReportController;
use \App\Http\Controllers\Users\RoleController;
use \App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group(['middleware' => 'api'], function ($router) {
    Route::group(['prefix' => 'auth'], function ($router) {
        Route::post('/login', [AuthController::class, 'login']);
        Route::post('/register', [AuthController::class, 'register']);
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::post('/change-pass', [AuthController::class, 'changePassWord']);
        Route::get('/get-list-user', [AuthController::class, 'getListUser']);
        Route::group(['prefix' => 'order'], function ($router) {
            Route::get('/car-rental/report', [Order\OrderController::class, 'report']);
            Route::put('/car-rental/deposit/{order}', [Order\OrderController::class, 'deposit']);
            Route::put('/car-rental/complete/{order}', [Order\OrderController::class, 'complete']);
            Route::get('/car-rental', [Order\OrderController::class, 'index']);
            Route::get('/car-rental/{order}', [Order\OrderController::class, 'show']);
            Route::put('/car-rental/{order}', [Order\OrderController::class, 'update']);
            Route::post('/car-rental', [Order\OrderController::class, 'store']);
            Route::delete('/car-rental/{order}', [Order\OrderController::class, 'destroy']);
            Route::post('/add-on-price', [Order\OrderController::class, 'addOnPrice']);
        });
        Route::group(['prefix' => 'dashboard'], function ($router) {
            Route::get('/report', [DashboardController::class, 'report']);
            Route::get('/report-chart', [DashboardController::class, 'reportChart']);
        });
        Route::group(['prefix' => 'report'], function () {
            Route::get('/car-rental', [ReportController::class, 'reportCarRental']);
        });
        Route::group(['prefix' => 'vehicle'], function ($router) {
            Route::get('/vehicles', [VehicleController::class, 'index']);
            Route::get('/vehicles/report', [VehicleController::class, 'report']);
            Route::post('/vehicles/store', [VehicleController::class, 'store']);
            Route::post('/vehicles/update', [VehicleController::class, 'update']);
            Route::delete('/vehicles/{vehicle}', [VehicleController::class, 'destroy']);
        });
        Route::group(['prefix' => 'order-sell'], function ($router) {
            Route::get('/', [OrderSellController::class, 'index']);
            Route::get('/report', [OrderSellController::class, 'report']);
            Route::post('/store', [OrderSellController::class, 'store']);
            Route::post('/update', [OrderSellController::class, 'update']);
            Route::delete('/{sellOrder}', [OrderSellController::class, 'destroy']);
        });
        Route::group(['prefix' => 'stores'], function () {
            Route::get('/all', [StoreController::class, 'all']);
            Route::get('/', [StoreController::class, 'index']);
            Route::post('/', [StoreController::class, 'store']);
            Route::get('/{store}', [StoreController::class, 'show']);
            Route::put('/{store}', [StoreController::class, 'update']);
            Route::delete('/{store}', [StoreController::class, 'destroy']);
        });
        Route::group(['prefix' => 'customers'], function () {
            Route::get('/', [CustomerController::class, 'index']);
            Route::post('/', [CustomerController::class, 'store']);
            Route::get('/search-by-id-card', [CustomerController::class, 'searchCustomerByIdCard']);
            Route::get('/{customer}', [CustomerController::class, 'show']);
            Route::put('/{customer}', [CustomerController::class, 'update']);
            Route::delete('/{customer}', [CustomerController::class, 'destroy']);
        });
        Route::group(['prefix' => 'transactions'], function () {
            Route::get('/', [TransactionController::class, 'index']);
            Route::delete('/{transaction}', [TransactionController::class, 'destroy']);
        });
        Route::group(['prefix' => 'priceVehicles'], function () {
            Route::get('/', [PriceVehicleController::class, 'index']);
            Route::post('/', [PriceVehicleController::class, 'storeOrUpdate']);
            Route::delete('/{priceVehicle}', [PriceVehicleController::class, 'destroy']);
        });
        Route::group(['prefix' => 'role'], function () {
            Route::get('/all', [RoleController::class, 'all']);
        });
        Route::group(['prefix' => 'users'], function () {
            Route::get('/', [UserController::class, 'index']);
            Route::get('/get-staff-by-store', [UserController::class, 'getStaffByStore']);
            Route::group(['middleware' => ['admin']], function () {
                Route::post('/store', [UserController::class, 'store']);
                Route::post('/update', [UserController::class, 'update']);
                Route::post('/change-password', [UserController::class, 'changePassword']);
                Route::delete('/{user}', [UserController::class, 'destroy']);
            });
        });
    });

    Route::post('/forgot-password', [UserController::class, 'forgotPassword']);
    Route::post('/reset-password', [UserController::class, 'resetPassword']);
});

Route::get('/verify-token', [AuthController::class, 'verifyToken'])->middleware('auth.jwt');


