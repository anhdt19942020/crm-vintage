export const status = [
    {
        id: 1,
        name: "Chờ duyệt"
    },
    {
        id: 2,
        name: "Chờ thanh toán"
    },
    {
        id: "using",
        name: "Đã hoàn tất"
    }
];

export const status_define = {
    1: "Chờ duyệt",
    2: "Chờ thanh toán",
    3: "Đã hoàn tất"
};

export const status_define_css = {
    1: "badge badge-secondary",
    2: "badge badge-primary",
    3: "badge badge-success",
};
