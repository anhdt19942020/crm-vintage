export const ORDER_STATUS = [
    {value: "pending", label: "Đang chờ duyệt"},
    {value: "renting", label: "Đang thuê"},
    {value: "wait_payment", label: "Chờ thanh toán"},
    {value: "completed", label: "Hoàn thành"},
    {value: "canceled", label: "Hủy"},
];
export const STATUS_COMPLETED = "completed";

export const ORDER_STATUS_DEFINE = {
    pending: "Đang chờ duyệt",
    renting: "Đang thuê",
    wait_payment: "Chờ thanh toán",
    completed: "Hoàn thành",
    canceled: "Hủy",
    wfpay: "wfpay",
};
export const ORDER_STATUS_DEFINE_CSS = {
    pending: "badge badge-secondary",
    renting: "badge badge-primary",
    wait_payment: "badge badge-warning",
    completed: "badge badge-success",
    canceled: "badge badge-danger",
    wfpay: "badge badge-danger",
};

export const THU = "in";
export const CHI = "out";
export const GIA_HAN_THEM = "addon";

export const HOAN_THANH = "completed";
export const TRANSACTION_TYPE = {
    in: "Thu",
    out: "Chi",
    addon: "Gia hạn thêm"
};

