import Vue from "vue";
import Vuex from "vuex";

import auth from "./auth.module";
import htmlClass from "./htmlclass.module";
import config from "./config.module";
import breadcrumbs from "./breadcrumbs.module";
import profile from "./profile.module";
import package1 from "./package.module";
import order from "./order.module";
import vehicle from "./vehicle.module";
import customers from "./customers.module";
import store from "./store.module";
import orderSell from "./orderSell.module";
import transaction from "./transaction.module";
import dashboard from "./dashboard.module";
import report from "./report.module";
import role from "./role.module";
import user from "./user.module";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        auth,
        htmlClass,
        config,
        breadcrumbs,
        profile,
        package1,
        order,
        vehicle,
        store,
        customers,
        orderSell,
        transaction,
        dashboard,
        report,
        role,
        user
    }
});
