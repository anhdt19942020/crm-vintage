import ApiService from "@/core/services/api.service";

// action types
export const VEHICLE_GET_ALL = "vehicle_get_all";
export const VEHICLE_GET_ALL_REPORT = "vehicle_get_all_report";
export const VEHICLE_CREATE = "vehicle_create";
export const VEHICLE_UPDATE = "vehicle_update";
export const VEHICLE_DELETE = "vehicle_delete";
export const PRICE_VEHICLES_INDEX = "price_vehicles_index";

export const PRICE_VEHICLES_UPDATE = "price_vehicles_update";
export const DELETE_PRICE_VEHICLES = "delete_price_vehicle";

// set

const state = {};

const getters = {};

const actions = {
    [VEHICLE_GET_ALL](context, credentials) {
        return new Promise((resolve, reject) => {
            ApiService.query("/api/auth/vehicle/vehicles", credentials)
                .then(({data}) => {
                    resolve(data);
                })
                .catch(({response}) => {
                    reject(response);
                });
        });
    },
    [VEHICLE_GET_ALL_REPORT](context, credentials) {
        return new Promise((resolve, reject) => {
            ApiService.query("/api/auth/vehicle/vehicles/report", credentials)
                .then(({data}) => {
                    resolve(data);
                })
                .catch(({response}) => {
                    reject(response);
                });
        });
    },
    [VEHICLE_CREATE](context, payload) {
        return new Promise((resolve, reject) => {
            ApiService.post("/api/auth/vehicle/vehicles/store", payload).then(({data}) => {
                resolve(data);
            }).catch(({response}) => {
                reject(response)
            });
        })
    },

    [VEHICLE_UPDATE](context, payload) {
        return new Promise((resolve, reject) => {
            ApiService.post('/api/auth/vehicle/vehicles/update', payload).then(({data}) => {
                resolve(data);
            }).catch(({response}) => {
                reject(response);
            });
        })
    },

    [PRICE_VEHICLES_INDEX](context, payload) {
        return new Promise((resolve, reject) => {
            ApiService.query('/api/auth/priceVehicles', payload).then(({data}) => {
                resolve(data);
            }).catch(({response}) => {
                reject(response);
            });
        })
    },
    /**
     * api update pricing vehicles
     * @param context
     * @param payload
     * @returns {Promise<unknown>}
     */
    [PRICE_VEHICLES_UPDATE](context, payload) {
        return new Promise((resolve, reject) => {
            ApiService.post('/api/auth/priceVehicles', payload).then(({data}) => {
                resolve(data);
            }).catch(({response}) => {
                reject(response);
            });
        })
    },

    /**
     * api delete setting price
     * @param context
     * @param id
     * @returns {Promise<unknown>}
     */
    [DELETE_PRICE_VEHICLES](context, id) {
        return new Promise((resolve, reject) => {
            ApiService.delete(`/api/auth/priceVehicles/${id}`, ).then(({data}) => {
                resolve(data);
            }).catch(({response}) => {
                reject(response);
            });
        })
    },


    [VEHICLE_DELETE](context, id) {
        return new Promise((resolve, reject) => {
            ApiService.delete(`/api/auth/vehicle/vehicles/${id}`)
                .then(({data}) => {
                    resolve(data);
                })
                .catch(({response}) => {
                    reject(response);
                });
        });
    },
};

const mutations = {};

export default {
    state,
    actions,
    mutations,
    getters
};
