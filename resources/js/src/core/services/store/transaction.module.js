import ApiService from "@/core/services/api.service";

// action types
export const TRANSACTION_GET_LIST = "transaction_get_list";
export const TRANSACTION_DELETE = "transaction_delete";

// set

const state = {};

const getters = {};

const actions = {
    [TRANSACTION_GET_LIST](context, credentials) {
        return new Promise((resolve, reject) => {
            ApiService.query("/api/auth/transactions", credentials)
                .then(({data}) => {
                    resolve(data);
                })
                .catch(({response}) => {
                    reject(response);
                });
        });
    },
    [TRANSACTION_DELETE](context, id) {
        return new Promise((resolve, reject) => {
            ApiService.delete(`/api/auth/transactions/${id}`)
                .then(({data}) => {
                    resolve(data);
                })
                .catch(({response}) => {
                    reject(response);
                });
        });
    },
};

const mutations = {};

export default {
    state,
    actions,
    mutations,
    getters
};
