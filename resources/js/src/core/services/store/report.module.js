import ApiService from "@/core/services/api.service";

// action types
export const REPORT_CAR_RENTAL = "report_car_rental";

// set

const state = {};

const getters = {};

const actions = {

    [REPORT_CAR_RENTAL](context, credentials) {
        return new Promise((resolve, reject) => {
            ApiService.query("/api/auth/report/car-rental", credentials)
                .then(({data}) => {
                    resolve(data);
                })
                .catch(({response}) => {
                    reject(response);
                });
        });
    },
};

const mutations = {};

export default {
    state,
    actions,
    mutations,
    getters
};
