import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    mode: "hash",
    routes: [
        {
            path: "/",
            redirect: "/dashboard",
            component: () => import("@/view/layout/Layout"),
            children: [
                {
                    path: "/dashboard",
                    name: "dashboard",
                    component: () => import("@/view/pages/Dashboard.vue")
                },
                {
                    path: "/transactions",
                    name: "transactions",
                    component: () => import("@/view/pages/finances/Transaction.vue")
                },
            ]
        },
        // Quản lý xe
        {
            path: "/",
            redirect: "/dashboard",
            component: () => import("@/view/layout/Layout"),
            children: [
                {
                    path: "/vehicles",
                    name: "vehicle",
                    component: () => import("@/view/pages/vehicles/VehicleIndex.vue")
                },
                {
                    path: "/customers",
                    name: "customers",
                    component: () => import("@/view/pages/customers/CustomerIndex.vue"),
                    children: []
                },
                {
                    path: "/customers-create",
                    name: "customers-create",
                    component: () => import("@/view/pages/customers/CustomerCreate.vue")
                },
                {
                    path: "/customers-update/:id",
                    name: "customers-update",
                    component: () => import("@/view/pages/customers/CustomerUpdate.vue")
                },
                {
                    path: "/stores",
                    name: "stores",
                    component: () => import("@/view/pages/stores/StoreIndex.vue"),
                    children: []
                },
                {
                    path: "/store-create",
                    name: "stores-create",
                    component: () => import("@/view/pages/stores/StoreCreate.vue")
                },
                {
                    path: "/store-update/:id",
                    name: "stores-update",
                    component: () => import("@/view/pages/stores/StoreUpdate.vue"),
                    children: []
                },
                {
                    path: "/pricing",
                    name: "pricing",
                    component: () => import("@/view/pages/prices/PriceIndex.vue")
                },
                {
                    path: "/user",
                    name: "user",
                    component: () => import("@/view/pages/users/UserIndex.vue")
                },
                {
                    path: "/car-rental",
                    name: "car-rental",
                    component: () => import("@/view/pages/Order/OrderCarRental.vue")
                },
                {
                    path: "/car-sell",
                    name: "car-sell",
                    component: () => import("@/view/pages/order-sell/OrderSellIndex.vue")
                },
                {
                    path: "/report/car-rental",
                    name: "report-car-rental",
                    component: () => import("@/view/pages/report/ReportCardRental.vue")
                }
            ]
        },
        {
            path: "/",
            component: () => import("@/view/pages/auth/Auth.vue"),
            children: [
                {
                    name: "login",
                    path: "/login",
                    component: () => import("@/view/pages/auth/Auth.vue")
                },
                {
                    name: "register",
                    path: "/register",
                    component: () => import("@/view/pages/auth/Auth.vue")
                },
                {
                    name: "ref",
                    path: "ref/:id",
                    component: () => import("@/view/pages/auth/Auth.vue")
                }
            ]
        },
        {
            path: "*",
            redirect: "/404"
        },
        {
            // the 404 route, when none of the above matches
            path: "/404",
            name: "404",
            component: () => import("@/view/pages/error/Error-1.vue")
        }
    ]
});
